<?php
/*
* Template Name: BLOG POSTS
*/
 get_header(); ?>

<section class="header__title d-flex j-center a-center b-black t-white p-relative">
	<h1><?php the_title(); ?></h1>
</section>
<section class="page__content">
  <div class="page__description w-50">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <?php the_content(); ?>
    <?php endwhile; endif; ?>
  </div>
</section>

<?php get_footer();?>