<?php
/*
* Template Name: contacto
*/
get_header()
?>
  <section class="form__template w-80 e-center t-center page top">
    <!-- <h1 class="title t-blue t-center title__page"><?php the_title(); ?></h1> -->
    <div class="row" style="margin-top:2rem;">
      <div class="col-lg-6 a-center j-center d-flex contacto" style="flex-direction:column;">
        <h2 class="t-bold">Para cualquier consulta,</h2>
        <!-- <br> -->
        <h2>comunicate con nosotros.</h2>
        <img class="imagen-flecha" src="<?php echo get_template_directory_uri(); ?>/assets/img/flecha.png" alt="" style="height:5rem; margin-top:1rem;">
      </div>
      <div class="col-lg-6">
        <div class="form__section">
          <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php get_footer()?>
