<?php get_header(); ?>
<!-- hero principal -->
<!-- cambios en slider hacerlos en ambos :)-->
<div id="carouselExampleControls1" class="carousel slide hero__principal container__padding slider-1" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item full-height active">
            <div class="hero__slide full-height"
                style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/slider/fondo.png'); background-position: center;background-size:cover;background-repeat: no-repeat;">
                <section class="hero__principal__container row a-center t-center w-70 e-center">
                    <figure class="hero__image col-lg-5">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/IPHONE.png" alt="">
                    </figure>
                    <section class="col-lg-7 d-flex j-center hero__description">
                        <div class="w-70">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/4.png" alt="">
                        </div>
                    </section>
                </section>
            </div>
        </div>
        <div class="carousel-item full-height">
            <div class="hero__slide full-height"
                style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/slider/fondo.png'); background-position: center;background-size:cover;background-repeat: no-repeat;">
                <section class="hero__principal__container row a-center t-center w-70 e-center">
                    <figure class="hero__image col-lg-5">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/huawei.png" alt="">
                    </figure>
                    <section class="col-lg-7 d-flex j-center hero__description">
                        <div class="w-70">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/texto foto 1.png"
                                alt="">
                        </div>
                    </section>
                </section>
            </div>
        </div>
        <div class="carousel-item full-height">
            <div class="hero__slide full-height"
                style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/slider/fondo.png'); background-position: center;background-size:cover;background-repeat: no-repeat;">
                <section class="hero__principal__container row a-center t-center w-70 e-center">
                    <figure class="hero__image col-lg-5">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/todos.png" alt="">
                    </figure>
                    <section class="col-lg-7 d-flex j-center hero__description">
                        <div class="w-70">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/texto3.png" alt="">
                        </div>
                    </section>
                </section>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!---slider solo telefonos-->
<div id="carouselExampleControls" class="carousel slide hero__principal container__padding slider-2"
    data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/FOTO 1.png"
                alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/FOTO 2 pagina web.png" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100"
                src="<?php echo get_template_directory_uri(); ?>/assets/img/FOTO 3 pagina web.png" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- hero secundario -->
<section class="hero__video container__padding ">
    <section class="hero__video__container row a-center t-center">
        <iframe width="100%" height="100%"
            src="https://www.youtube.com/embed/ottKO-7t-A4?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"
            allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </section>
</section>
<!-- Option item -->
<section class="option row" id="servicios">
    <section class="col-lg-6 item__padding p-relative">
        <section class="item__container full-height">
            <figure class="item__img">
                <a href=src="<?php echo get_template_directory_uri(); ?>/tratamiento-de-dispositivos-mojados">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mojado.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_one">
                <h3 class="t-white">¿Mojaste tu teléfono?</h3>
                <h2 class="service__title title t-white b-bold">Nosotros lo reparamos</h2>
            </div>
        </section>
    </section>
    <section class="col-lg-6 item__padding">
        <section class="t-center item__container full-height p-relative d-flex j-center a-center">
            <figure class="item__img full-width full-height">
                <a href=src="<?php echo get_template_directory_uri(); ?>/cambio-de-puerto-de-carga">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cargador.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_two full-width b-blue">
                <h2 class="service__title title t-white b-bold">¿Necesitas cargador nuevo?</h2>
                <h3 class="t-white">Accesorios para tu teléfono</h3>
            </div>
        </section>
    </section>
    <section class="col-lg-6 item__padding">
        <section class="t-center item__container full-height p-relative d-flex j-center a-center">

            <figure class="item__img full-width full-height">
                <a href=src="<?php echo get_template_directory_uri(); ?>/cambio-de-bateria">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/baterias.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_two full-width b-blue">
                <h2 class="service__title title t-white b-bold">Cambiamos tu batería</h2>
            </div>

        </section>
    </section>
    <section class="col-lg-6 item__padding p-relative">
        <section class="item__container full-height d-flex j-end a-center">

            <figure class="item__img full-width full-height">
                <a href=src="<?php echo get_template_directory_uri(); ?>/cambio-de-pantalla-glass">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/glass.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_three t-right">
                <h2 class="service__title title t-white b-bold">Cambiamos el <br>Glass de tu <br>teléfono</h2>
            </div>

        </section>
    </section>
    <section class="col-lg-6 item__padding">
        <section class="item__container full-height p-relative full-width full-height">

            <figure class="item__img full-width full-height">
                <a href=src="<?php echo get_template_directory_uri(); ?>/cambio-de-pantalla-rota">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rota.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_one">
                <h3 class="t-white">¿Pantalla rota?</h3>
                <h2 class="service__title title t-white b-bold">Nosotros la reparamos</h2>
            </div>

        </section>
    </section>
    <section class="col-lg-6 item__padding">
        <section class="t-center item__container p-relative full-height d-flex j-center a-top">

            <figure class="item__img full-width full-height">
                <a href=src="<?php echo get_template_directory_uri(); ?>/cambio-de-pantalla-rota">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/accesorio.png" alt="">
                </a>
            </figure>
            <div class="p-absolute message_two message_four b-blue full-width">
                <h3 class="t-white">¿Pantalla rota?</h3>
                <h2 class="service__title title t-white b-bold">Nosotros la reparamos</h2>
            </div>

        </section>
    </section>

</section>
<!-- Modal -->

<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer()?>