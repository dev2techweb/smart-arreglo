<?php
/*
* Template Name: catalogo
*/
get_header()
?>
  <section class="page top">
    <h1 class="title t-blue t-center title__page"><?php the_title(); ?></h1>
    <div class="t-center">
      <a href="<?php the_field('link_de_descarga');?>" class="btn b-blue t-white b__green-hover t-white__hover btn__download"> Descargar </a>
      <style>.embed-container { position: relative; padding-bottom:56.25%; height:0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%;}</style>
      <div class="embed-container" data-page-width="695" data-page-height="840" id="ypembedcontainer" style="width: 100%; height: 100%;">
        <iframe src="<?php the_field('link_del_embed');?>" frameborder="0" allowfullscreen="true" allowtransparency="true"></iframe>
      </div>
      <p><script src="https://players.yumpu.com/modules/embed/yp_r_iframe.js"></script></p>
    </div>
  </section>
<?php get_footer()?> 